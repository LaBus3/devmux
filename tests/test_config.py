# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Test for DevMux configuration module

Use Nose2 to run this from root of source tree"""

import unittest
import os
import shutil
from devmux.common.config import Config


class DevMuxTestSuite(unittest.TestCase):
    """Test suite for DevMux configuration"""

    def setUp(self):
        path = Config.default_file_path()
        self.toml_existed = os.path.exists(path)
        if not self.toml_existed:
            directory = os.path.dirname(path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            src = os.path.dirname(__file__) + '/../sample_configs/devmux.toml'
            dst = path
            shutil.copy(src, dst)

    def tearDown(self):
        path = Config.default_file_path()
        if not self.toml_existed:
            os.remove(path)

    def test_config_load_none(self):
        """Test config loader to load from XDG default"""
        Config()

    def test_config_load_arbitarary(self):
        """Test config loader to load arbitarary configuration"""
        path = './sample_configs/devmux.toml'
        Config(path)

    def test_config_fail_on_file_error(self):
        """Test config loader to raise exception for non-existant file"""
        with self.assertRaises(FileNotFoundError):
            Config('foo.bar')
