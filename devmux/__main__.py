# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Command processor for dmux invocation"""

import sys


if len(sys.argv) > 1:
    command = sys.argv[1]
else:
    command = None
if len(sys.argv) > 2:
    args = ' '.join(sys.argv[2:])
    # Prefix space is for padding while passing arguments
else:
    args = ''

if command == 'init':
    pass
elif command == 'attach':
    pass
elif command is None:
    # Launch session
    pass
elif command == 'develop':
    # Error if not inside session
    # Start develop window
    # In current folder if path not given, else in given path
    # Label window serially
    pass
elif command == 'gdb':
    # Error if not inside session
    # Start gdb window with dashboards. Pass arguments verbatim
    # Label window serially
    pass
