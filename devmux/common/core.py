# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Core common functions of DevMux"""

import libtmux as tmux


def get_session():
    """Get handle for DevMux session

    Returns None if session is not available"""
    server = tmux.Server()
    if server.has_session('DevMux'):
        return server.find_where({'session_name': 'DevMux'})
    else:
        return None


def populate():
    """Populate Session with windows

    Called just after session initialization.
    The configurtion is taken from config file"""
    pass


def init():
    """Initialize TMux server DevMux session

    Will do nothing for server/session already setup.
    Wont attach session"""
    pass


def attach():
    """Attach an already available DevMux session

    Will do nothing if session is alrewady attached.
    Will error if session is not available"""
    pass


def launch():
    """Launch session and attach

    Initializes server and session. Will do nothing if already done.
    Attaches session. Will do nothing if already done"""
    pass
