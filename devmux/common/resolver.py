# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Path resolver for DevMux"""

import os


def resolve_path(path):
    """Resolver for paths in arguments

    Returns current directory if empty"""
    if path == '':
        path = os.getcwd()
    path = os.path.expanduser(path)
    path = os.path.expandvars(path)
    return os.path.abspath(path)
